package br.com.bacci.observer.consumer;

public interface Consumer {

	void onSuccess(String text);
	void onFailure(String err);
	
}

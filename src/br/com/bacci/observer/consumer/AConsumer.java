package br.com.bacci.observer.consumer;

public class AConsumer implements Consumer {

	private static final String NOME_CONSUMER = " - Consumer A";
	
	@Override
	public void onSuccess(String text) {
		System.out.println(text + NOME_CONSUMER);
	}

	@Override
	public void onFailure(String err) {
		System.out.println(err + NOME_CONSUMER);
	}

}

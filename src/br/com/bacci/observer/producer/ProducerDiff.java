package br.com.bacci.observer.producer;

import java.util.Random;

import br.com.bacci.observer.consumer.Consumer;

public class ProducerDiff {

	private Random rd;
	
	public ProducerDiff() {
		this.rd = new Random();
	}
	
	public void notifySuccess(Consumer consumer, String text) {
		consumer.onSuccess(text);
	}
	
	public void notifyFailure(Consumer consumer, String err) {
		consumer.onFailure(err);
	}
	
	public void process(Consumer consumer) {
		if (this.rd.nextBoolean()) {
			this.notifySuccess(consumer, "Dif - Parabéns, sucesso");
		} else {
			this.notifyFailure(consumer, "Dif - Paaaammmm, erro");
		}
	}
}

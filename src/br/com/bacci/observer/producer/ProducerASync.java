package br.com.bacci.observer.producer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import br.com.bacci.observer.consumer.Consumer;

public class ProducerASync {

	private List<Consumer> consumers;
	private Random rd;
	
	public ProducerASync() {
		this.consumers = new ArrayList<Consumer>();
		this.rd = new Random();
	}
	
	public void addConsumers(Consumer consumer) {
		if (Objects.nonNull(consumer)) {
			this.consumers.add(consumer);
		}
	}
	
	public void notifySuccess(String text) {
		this.consumers.forEach(consumer -> consumer.onSuccess(text));
	}
	
	public void notifyFailure(String err) {
		this.consumers.forEach(consumer -> consumer.onFailure(err));
	}
	
	public void process() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				if (rd.nextBoolean()) {
					notifySuccess("ASync - Parabéns, sucesso");
				} else {
					notifyFailure("ASync - Paaaammmm, erro");
				}
			}
		}).start();
	}
}

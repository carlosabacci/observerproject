package br.com.bacci.observer.producer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import br.com.bacci.observer.consumer.Consumer;

public class ProducerSync {

	private List<Consumer> consumers;
	private Random rd;
	
	public ProducerSync() {
		this.consumers = new ArrayList<Consumer>();
		this.rd = new Random();
	}
	
	public void addConsumers(Consumer consumer) {
		if (Objects.nonNull(consumer)) {
			this.consumers.add(consumer);
		}
	}
	
	public void notifySuccess(String text) {
		this.consumers.forEach(consumer -> consumer.onSuccess(text));
	}
	
	public void notifyFailure(String err) {
		this.consumers.forEach(consumer -> consumer.onFailure(err));
	}
	
	public void process() {
		if (this.rd.nextBoolean()) {
			this.notifySuccess("Sync - Parabéns, sucesso");
		} else {
			this.notifyFailure("Sync - Paaaammmm, erro");
		}
	}
}

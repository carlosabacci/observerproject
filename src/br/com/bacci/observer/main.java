package br.com.bacci.observer;

import br.com.bacci.observer.consumer.AConsumer;
import br.com.bacci.observer.consumer.BConsumer;
import br.com.bacci.observer.consumer.Consumer;
import br.com.bacci.observer.producer.ProducerASync;
import br.com.bacci.observer.producer.ProducerDiff;
import br.com.bacci.observer.producer.ProducerSync;

public class main {

	public static void main(String[] args) {
		Consumer ac = new AConsumer();
		Consumer bc = new BConsumer();
		
		// Sincrono
		ProducerSync prd = new ProducerSync();
		
		prd.addConsumers(ac);
		prd.addConsumers(bc);
		
		for (int i = 0; i < 10; i++) {
			prd.process();
		}
		
		// Asincrono
		ProducerASync prdA = new ProducerASync();
		
		prdA.addConsumers(ac);
		prdA.addConsumers(bc);
		
		for (int i = 0; i < 10; i++) {
			prdA.process();
		}
		
		// diferente
		ProducerDiff dif = new ProducerDiff();
		
		for (int i = 0; i < 10; i++) {
			dif.process(new Consumer() {
				
				@Override
				public void onSuccess(String text) {
					System.out.println(text + " - Criado na classe");
				}
				
				@Override
				public void onFailure(String err) {
					System.out.println(err + " - Criado na classe");
				}
			});
		}
	}

}
